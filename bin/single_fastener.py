# system packages
import sys
import timeit
from datetime import datetime
import logging as lg

# science packages
import numpy as np
import pandas as pd

# timber connection package
from pyconnect.core import size_effect
from pyconnect.inputs import LoadInputs
from pyconnect.material import Parameters
from pyconnect.connection import Connections
from pyconnect.plotting import plot_sf_results


lg.basicConfig(level=lg.INFO, filename='main.log')


def main(fname, N):
    lg.info('load connection geometries ------------------------')
    inputs = LoadInputs(fname)
    inputs.load_geom_params()
    inputs.load_mat_params()
    # sort connections array acc. to volume
    inputs.connections = inputs.connections[inputs.connections[:, -1].argsort()]
    lg.info('connection geometries loaded successfully! --------')

    for idn, n in enumerate(N):
        lg.info('load material properties --------------------------')
        m = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=n)
        m.generate_random_variates()
    
        m_se = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=n)
        m_se.generate_random_variates()
        lg.info('material properties loaded successfully! ----------')
    
        lg.info('compute connection capacities ---------------------')
    
        initial_guess = [0.42912284071327017, 0.22015699829905505]
        initial_guess_store = np.zeros((inputs.connections.shape[0], 2))
        advanced = 'yes'

        now = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
    
        data1 = np.zeros((inputs.connections.shape[0], 7))
        data2 = np.zeros((inputs.connections.shape[0], 7))
    
        c = Connections(inputs, samples=n)
        c_se = Connections(inputs, samples=n)

        for i in range(inputs.connections.shape[0]):
            
            c.i = i
            c.N = n
            c.compute_capacity(inputs.connections[i, :], m, advanced)
            lg.info('connection capacities computed successfully -------')
    
            data1[:, 0] = c.probability.reshape((-1,))
            data1[:, 1] = c.R_mean.reshape((-1,))
            data1[:, 2] = c.R_cov.reshape((-1,))
            data1[:, 3] = c.Rd_mean.reshape((-1,))
            data1[:, 4] = c.Rd_cov.reshape((-1,))
            data1[:, 5] = c.Rb_mean.reshape((-1,))
            data1[:, 6] = c.Rb_cov.reshape((-1,))
            
            V = inputs.connections[i, -1]
            # tf = initial_guess_mat[:, 2] == V
            # initial_guess = initial_guess_mat[tf, 0:2].reshape((-1,))
            m_se.rand_variates[:, -1], initial_guess = size_effect(V, m_se,
                                                                   initial_guess)
            initial_guess_store[i, :] = initial_guess
    
            c_se.i = i
            c_se.N = n
            c_se.compute_capacity(inputs.connections[i, :], m_se, advanced)
            lg.info('connection capacities computed successfully -------')
    
            data2[:, 0] = c.probability.reshape((-1,))
            data2[:, 1] = c.R_mean.reshape((-1,))
            data2[:, 2] = c.R_cov.reshape((-1,))
            data2[:, 3] = c.Rd_mean.reshape((-1,))
            data2[:, 4] = c.Rd_cov.reshape((-1,))
            data2[:, 5] = c.Rb_mean.reshape((-1,))
            data2[:, 6] = c.Rb_cov.reshape((-1,))

        # create pandas dataframes of results data
        names = list(inputs.geom_params.keys())
        names += ['b', 'V', 'P(B<D)', 'Mean', 'COV',
                  'Mean Ductile', 'COV Ductile',
                  'Mean Brittle', 'COV Brittle']
        
        c.data1 = np.hstack((inputs.connections, data1))
        df1 = pd.DataFrame(c.data1, columns=names)
        
        c.data2 = np.hstack((inputs.connections, data2))
        df2 = pd.DataFrame(c.data2, columns=names)
        
        # plot probability of brittle < ductile failure
        keyid = sorted(range(len(inputs.geom_params_length)),
                       key=lambda i: inputs.geom_params_length[i],
                       reverse=True)[:2]
        fig, fig1 = plot_sf_results(df1, df2, inputs.geom_params, keyid)
        fig.savefig('../results/%s.pdf' % inputs.name)
        fig1.savefig('../results/%s-onesubplot.pdf' % inputs.name)

        # write data to csv files
        now = datetime.now().strftime('%Y-%m-%d-%H:%M')
        out_fname = '../results/%s-%s.csv' % (now, inputs.name)
        df1.to_csv(out_fname)
        
        now = datetime.now().strftime('%Y-%m-%d-%H:%M')
        out_fname = '../results/%s-%s-size-effect.csv' % (now, inputs.name)        
        df2.to_csv(out_fname)

    return(inputs, m, c, initial_guess_store)


if __name__ == '__main__':
    lg.getLogger(__name__)

    # define input filenames containing geometric parameters
    try:
        filenames = sys.argv[1]
    except(IndexError):
        filenames = ['series-probabilityBlessD-SF-vary-t1.json',
                     'series-probabilityBlessD-SF-vary-a3.json']
        #['../data/example-input-series-3.json']
    
    start = timeit.default_timer()
    
    # define other input parameters
    N = [1000]  # number of realizations to sample material properties with
    # num_subplots = 5  # number of subplots in failure distribution figure
    
    
    for fname in filenames:
        inputs, m, c, initial_guess_store = main(fname, N)

    stop = timeit.default_timer()

    total_time = stop - start

    # output running time in a nice format.
    mins, secs = divmod(total_time, 60)
    hours, mins = divmod(mins, 60)

    print("Total running time: %d:%d:%d.\n" % (hours, mins, secs))
