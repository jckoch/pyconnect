#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 28 20:36:00 2020

@author: jkoch
"""

import numpy as np
from scipy.stats import norm, lognorm, weibull_min
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
from pyconnect.inputs import LoadInputs
from pyconnect.material import Parameters
from pyconnect.connection import Connections


# %% generate random variates
fname = '../data/plot-distributions-sample-input.json'

N = 1000

inputs = LoadInputs(fname)
inputs.load_mat_params()

m = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=1000)
m.generate_random_variates()
m_se = Parameters(inputs.mat_params, inputs.correlation_matrix,
                  samples=N)
m_se.generate_random_variates()

# %% define pdf of timber material properties
dp = m.mat_params

mu=dp['rho']['mu']
sigma=dp['rho']['sigma']
x0 = np.linspace(norm(mu, sigma).ppf(0.001),
                 norm(mu, sigma).ppf(0.999), N)
rv0 = norm(mu, sigma)

mu=dp['fv']['mu']
sigma=dp['fv']['sigma']
x2 = np.linspace(lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.001),
                 lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.999), N)
rv2 = lognorm(sigma, loc=0, scale=np.exp(mu))

mu=dp['ft0']['mu']
sigma=dp['ft0']['sigma']
x3 = np.linspace(lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.001),
                 lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.999), N)
rv3 = lognorm(sigma, loc=0, scale=np.exp(mu))

k=dp['ft90']['k']
Lambda=dp['ft90']['lambda']
x4 = np.linspace(weibull_min(k, loc=0, scale=Lambda).ppf(0.001),
                 weibull_min(k, loc=0, scale=Lambda).ppf(0.999), N)
rv4 = weibull_min(k, loc=0, scale=Lambda)

# %% plot timber material property distributions
RV = m.rand_variates

fig1, axes1 = plt.subplots(nrows=2, ncols=2, figsize=(9.3, 7.4))
fig1a, ax1a = plt.subplots(nrows=1, ncols=2, figsize=(9.3, 3.7))
fig1b, ax1b = plt.subplots(nrows=1, ncols=2, figsize=(9.3, 3.7))

axes1[0, 0].hist(RV[:, 0], bins=100, color='k', alpha=0.25, density=True)
axes1[0, 0].plot(x0, rv0.pdf(x0), color='k', lw=4)
axes1[0, 0].set_xlabel('$\\rho$ [$kg/m^3$]')
axes1[0, 0].set_ylabel('Relative frequency [-]')
string = '$\\rho_{mean} = %3.0f kg/m^3$\n$\\rho_k = %3.0f kg/m^3$' % (rv0.ppf(0.5), rv0.ppf(0.05))
axes1[0, 0].text(0.8, 0.8, string, size=10, ha="center", va="center", transform=axes1[0, 0].transAxes)
ax1a[0].hist(RV[:, 0], bins=100, color='k',
                alpha=0.25, density=True)
ax1a[0].plot(x0, rv0.pdf(x0), color='k', lw=4)
ax1a[0].set_xlabel('$\\rho$ [$kg/m^3$]')
ax1a[0].set_ylabel('Relative frequency [-]')
ax1a[0].text(0.75, 0.8, string, size=12, ha="center", va="center", transform=ax1a[0].transAxes)

axes1[0, 1].hist(RV[:, 2], bins=100, color='k', alpha=0.25, density=True)
axes1[0, 1].plot(x2, rv2.pdf(x2), color='k', lw=4)
axes1[0, 1].set_xlabel('$f_v$ [$MPa$]')
axes1[0, 1].set_ylabel('Relative frequency [-]')
string = '$f_{v,mean} = %1.1f MPa$\n$f_{v,k} = %1.1f MPa$' % (rv2.ppf(0.5), rv2.ppf(0.05))
axes1[0, 1].text(0.8, 0.8, string, size=10, ha="center", va="center", transform=axes1[0, 1].transAxes)
ax1a[1].hist(RV[:, 2], bins=100, color='k',
             alpha=0.25, density=True)
ax1a[1].plot(x2, rv2.pdf(x2), color='k', lw=4)
ax1a[1].set_xlabel('$f_v$ [$MPa$]')
ax1a[1].set_ylabel('Relative frequency [-]')
ax1a[1].text(0.8, 0.8, string, size=12, ha="center", va="center", transform=ax1a[1].transAxes)

axes1[1, 0].hist(RV[:, 3], bins=100, color='k', alpha=0.25, density=True)
axes1[1, 0].plot(x3, rv3.pdf(x3), color='k', lw=4)
axes1[1, 0].set_xlabel('$f_{t,\\parallel}$ [$MPa$]')
axes1[1, 0].set_ylabel('Relative frequency [-]')
string = '$f_{t,\parallel,mean} = %2.0f MPa$\n$f_{t,\parallel,k} = %2.0f MPa$' % (rv3.ppf(0.5), rv3.ppf(0.05))
axes1[1, 0].text(0.8, 0.8, string, size=10, ha="center", va="center", transform=axes1[1, 0].transAxes)
ax1b[0].hist(RV[:, 3], bins=100, color='k', alpha=0.25, density=True)
ax1b[0].plot(x3, rv3.pdf(x3), color='k', lw=4)
ax1b[0].set_xlabel('$f_{t,\\parallel}$ [$MPa$]')
ax1b[0].set_ylabel('Relative frequency [-]')
ax1b[0].text(0.75, 0.8, string, size=12, ha="center", va="center", transform=ax1b[0].transAxes)

axes1[1, 1].hist(RV[:, 4], bins=100, color='k', alpha=0.25, density=True)
axes1[1, 1].plot(x4, rv4.pdf(x4), color='k', lw=4)
axes1[1, 1].set_xlabel('$f_{t,\perp}$ [$MPa$]')
axes1[1, 1].set_ylabel('Relative frequency [-]')
string = '$f_{t,\perp,mean} = %1.1f MPa$\n$f_{t,\perp,k} = %1.1f MPa$' % (rv4.ppf(0.5), rv4.ppf(0.05))
axes1[1, 1].text(0.8, 0.8, string, size=10, ha="center", va="center", transform=axes1[1, 1].transAxes)
ax1b[1].hist(RV[:, 4], bins=100, color='k', alpha=0.25, density=True)
ax1b[1].plot(x4, rv4.pdf(x4), color='k', lw=4)
ax1b[1].set_xlabel('$f_{t,\perp}$ [$MPa$]')
ax1b[1].set_ylabel('Relative frequency [-]')
ax1b[1].text(0.25, 0.8, string, size=12, ha="center", va="center", transform=ax1b[1].transAxes)

fig1.tight_layout()
fig1.savefig('../../../thesis/figure/timber-material-property-distributions.pdf')
fig1a.tight_layout()
fig1a.savefig('../../../thesis_presentation/figure/timber-material-property-distributions-01.pdf')
fig1b.tight_layout()
fig1b.savefig('../../../thesis_presentation/figure/timber-material-property-distributions-02.pdf')

# %% computed derived material properties: timber
# for a diameter of fastener equal to 12mm

inputs.load_geom_params()
c = Connections(inputs)
c.i = 0
c.N = N
c.compute_capacity(inputs.connections[0, :], m, 'yes')

RV_derived = c.rv

# %% plot derived material properties: timber

fig2, axes2 = plt.subplots(nrows=1, ncols=2, figsize=(9.3, 3.7))
fig2a, axes2a = plt.subplots(nrows=1, ncols=1, figsize=(9.3, 3.7))
fig2b, axes2b = plt.subplots(nrows=1, ncols=1, figsize=(9.3, 3.7))

axes2[0].hist(RV_derived[:, 5], bins=100, color='k',
              alpha=0.25, density=True)
mu, sigma = [np.mean(RV_derived[:, 5]), np.std(RV_derived[:, 5])]
x5 = np.linspace(norm(mu, sigma).ppf(0.001),
                 norm(mu, sigma).ppf(0.999), 1000)
rv5 = norm(mu, sigma)
axes2[0].plot(x5, rv5.pdf(x5), color='k', lw=4)
axes2[0].set_xlabel('$f_h$ [$MPa$]')
axes2[0].set_ylabel('Relative frequency [-]')
string = '$f_{h,mean} = %2.0f MPa$\n$f_{h,k} = %2.0f MPa$' % (rv5.ppf(0.5), rv5.ppf(0.05))
axes2[0].text(0.8, 0.8, string, size=12, ha="center", va="center", transform=axes2[0].transAxes)
axes2a.hist(RV_derived[:, 5], bins=100, color='k',
            alpha=0.25, density=True)
axes2a.plot(x5, rv5.pdf(x5), color='k', lw=4)
axes2a.set_xlabel('$f_h$ [$MPa$]')
axes2a.set_ylabel('Relative frequency [-]')
string = '$f_{h,mean} = %2.0f MPa$\n$f_{h,k} = %2.0f MPa$' % (rv5.ppf(0.5), rv5.ppf(0.05))
axes2a.text(0.8, 0.8, string, ha="center", va="center", transform=axes2a.transAxes)

axes2[1].hist(RV_derived[:, 6]/1000, bins=100, color='k',
              alpha=0.25, density=True)
mu, sigma = [np.mean(RV_derived[:, 6])/1000, np.std(RV_derived[:, 6])/1000]
x6 = np.linspace(norm(mu, sigma).ppf(0.001),
                 norm(mu, sigma).ppf(0.999), 1000)
rv6 = norm(mu, sigma)
axes2[1].plot(x6, rv6.pdf(x6), color='k', lw=4)
axes2[1].set_xlabel('$M_y$ [$kNm$]')
axes2[1].set_ylabel('Relative frequency [-]')
string = '$M_{y,mean} = %3.0f kNm$\n$M_{y,k} = %3.0f kNm$' % (rv6.ppf(0.5), rv6.ppf(0.05))
axes2[1].text(0.75, 0.8, string, size=12, ha="center", va="center", transform=axes2[1].transAxes)

axes2b.hist(RV_derived[:, 6]/1000, bins=100, color='k',
            alpha=0.25, density=True)
axes2b.plot(x6, rv6.pdf(x6), color='k', lw=4)
axes2b.set_xlabel('$M_y$ [$kNm$]')
axes2b.set_ylabel('Relative frequency [-]')
string = '$M_{y,mean} = %3.0f kNm$\n$M_{y,k} = %3.0f kNm$' % (rv6.ppf(0.5), rv6.ppf(0.05))
axes2b.text(0.75, 0.8, string, ha="center", va="center", transform=axes2b.transAxes)

fig2.tight_layout()
fig2.savefig('../../../thesis/figure/timber-material-derived-property-distributions.pdf')
fig2a.tight_layout()
fig2a.savefig('../../../thesis_presentation/figure/timber-material-fh-property-distributions.pdf')
fig2b.tight_layout()
fig2b.savefig('../../../thesis_presentation/figure/timber-material-My-property-distributions.pdf')

# %% define pdf of steel fastener material properties
mu=dp['fu']['mu']
sigma=dp['fu']['sigma']
x1 = np.linspace(lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.001),
                 lognorm(sigma, loc=0, scale=np.exp(mu)).ppf(0.999), 1000)
rv1 = lognorm(sigma, loc=0, scale=np.exp(mu))

# %% plot steel material property distributions
fig3, axes3 = plt.subplots(nrows=1, ncols=1, figsize=(9.3, 4.7))

axes3.hist(RV[:, 1], bins=100, color='k',
                alpha=0.25, density=True)
axes3.plot(x1, rv1.pdf(x1), color='k', lw=4)
axes3.set_xlabel('$f_u$ [MPa]')
axes3.set_ylabel('Relative frequency [-]')
string = '$f_{u,mean} = %3.0f MPa$\n$f_{u,k} = %3.0f MPa$' % (rv1.ppf(0.5), rv1.ppf(0.05))
axes3.text(0.8, 0.8, string, size=12, ha="center", va="center", transform=axes3.transAxes)

fig3.tight_layout()
fig3.savefig('../../../thesis/figure/steel-fastener-ultimate-strength-distribution.pdf')
