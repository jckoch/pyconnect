# system packages
import sys
import timeit
import logging as lg

# science packages
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt

# timber connection package
from pyconnect.core import size_effect
from pyconnect.inputs import LoadInputs
from pyconnect.material import Parameters
from pyconnect.connection import Connections

lg.basicConfig(level=lg.INFO, filename='main.log')


def main(fname, N):
    lg.info('load connection geometries ------------------------')
    inputs = LoadInputs(fname)
    inputs.load_geom_params()
    inputs.load_mat_params()
    lg.info('connection geometries loaded successfully! --------')

    for idn, n in enumerate(N):
        lg.info('load material properties --------------------------')
        m = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=1000)
        m.generate_random_variates()
    
        m_se = Parameters(inputs.mat_params, inputs.correlation_matrix,
                          samples=1000)
        m_se.generate_random_variates()
        lg.info('material properties loaded successfully! ----------')
    
        lg.info('compute connection capacities ---------------------')
        initial_guess = [0.42912284071327017, 0.22015699829905505]
        initial_guess_store = np.zeros((inputs.connections.shape[0], 2))
        advanced = 'yes'
    
        c = Connections(inputs, samples=1000)
        c_se = Connections(inputs, samples=1000)

        row = 0
        col = 0

        lengths = np.array(inputs.geom_params_length)
        R, C = lengths[lengths != 1]

        fig1, ax1 = plt.subplots(nrows=R, ncols=C, figsize=(9.3, 7.4))
        for i in range(inputs.connections.shape[0]):
            
            c.i = i
            c.N = n
            c.compute_capacity(inputs.connections[i, :], m, advanced) 
            
            var = inputs.connections[i, 0]
            titlestr = 'Constant geometric parameters\n$\\frac{a_3}{d} =$ %.2f and $\\frac{a_4}{d} =$ %.2f' % (
                (inputs.connections[i, 7] / inputs.connections[i, 0]),
                (inputs.connections[i, 8] / inputs.connections[i, 0]))
    
            kde = gaussian_kde(c.R[:, c.i]/1000)
            density = kde(c.R[:, c.i]/1000)
            
            ax1[row, col].hist(c.R[:, c.i]/1000, bins=100, color='k',
                               alpha=0.25, density=True,
                               orientation='horizontal')
            ax1[row, col].plot(density, c.R[:, c.i]/1000, 'k')

            V = inputs.connections[i, -1]
            m_se.rand_variates[:, -1], initial_guess = size_effect(V, m_se,
                                                                   initial_guess)
            initial_guess_store[i, :] = initial_guess
    
            c_se.i = i
            c_se.N = n
            c_se.compute_capacity(inputs.connections[i, :], m_se, advanced)
            
            kde_se = gaussian_kde(c_se.R[:, c_se.i])
            density_se = kde_se(c_se.R[:, c_se.i])
    
            if inputs.connections[i, -1] > 0.075:
                ax1[row, col].hist(c_se.R[:, c_se.i]/1000, bins=100,
                                   color='k', alpha=0.25, density=True,
                                   orientation='horizontal')
                ax1[row, col].plot(density_se, c_se.R[:, c_se.i]/1000, 'k')

                ax1[row, col].set_xticks([], [])
                ax1[row, col].set_xlabel('$d = $ %.2f mm' % var)

            if i % 2 == 0:
                col += 1
            else:
                row += 1
                col = 0
          
            ax1[0, 0].set_title('$\\frac{t_{1,3}}{d} = $ %.2f' % inputs.geom_params['t1'][0])
            ax1[0, 1].set_title('$\\frac{t_{1,3}}{d} = $ %.2f' % inputs.geom_params['t1'][1])
            ax1[0, 0].set_ylabel('$R_{connection}$ [kN]')
            ax1[1, 0].set_ylabel('$R_{connection}$ [kN]')
            ax1[2, 0].set_ylabel('$R_{connection}$ [kN]')
            ax1[0, 0].set_title('$\\frac{t_{1,3}}{d} = $ %.2f' % inputs.geom_params['t1'][0])
            ax1[0, 1].set_title('$\\frac{t_{1,3}}{d} = $ %.2f' % inputs.geom_params['t1'][1])
            ax1[0, 0].set_ylabel('$R_{connection}$ [kN]')

        fig1.suptitle(titlestr)
        fig1.savefig('../results/%s.pdf' % inputs.name)
    
        lg.info('connection capacities computed successfully -------')

    return(inputs, m, c, initial_guess_store)


if __name__ == '__main__':
    lg.getLogger(__name__)

    # define input filenames containing geometric parameters
    try:
        filenames = sys.argv[1]
    except(IndexError):
        filenames = ['series-pickdistplots-SF-vary-t1.json']
        #['../data/example-input-series-2.json']

    start = timeit.default_timer()
    
    N = [1000]
    
    for fname in filenames:
        inputs, m, c, initial_guess_store = main(fname, N)

    stop = timeit.default_timer()

    total_time = stop - start

    # output running time in a nice format.
    mins, secs = divmod(total_time, 60)
    hours, mins = divmod(mins, 60)

    print("Total running time: %d:%d:%d.\n" % (hours, mins, secs))


