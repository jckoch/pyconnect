# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
from scipy.stats import weibull_min
from pyconnect.core import weibull_parameters

V = np.array([0.001, 0.01, 0.1])
V0 = 0.01
f0 = 1.1
k0 = 4.542213092139039

m = f0 / (V / V0)**(1 / k0)
cov = 0.25
v = (cov * m)**2

linestyles = ['--', '-', '-.']
labels = ['$V =$ %.3f $m^3$; $f_{mean} =$ %1.1f $MPa$' % (V[0], m[0]),
          '$V_0 =$ %.3f $m^3$; $f_{0,mean} =$ %1.1f $MPa$' % (V[1], m[1]),
          '$V =$ %.3f $m^3$; $f_{mean} =$ %1.1f $MPa$' % (V[2], m[2])]

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(9.3, 7.4))

for i in range(m.shape[0]):
    Lambda, k, _ = weibull_parameters(m[i], v[i])
    x = np.linspace(weibull_min.ppf(0.001, k, loc=0, scale=Lambda),
                    weibull_min.ppf(0.999, k, loc=0, scale=Lambda), 100)
    # variate = weibull_min(k, loc=0, scale=Lambda).pdf(x)
    ax.plot(x, weibull_min(k, loc=0, scale=Lambda).pdf(x),
            'k', linestyle=linestyles[i], lw=2, alpha=0.6,
            label=labels[i])
    ax.hist(weibull_min(k, loc=0, scale=Lambda).rvs(1000), bins=50,
            color='k', alpha=0.1, density=True)

ax.set_xlabel('Material strength (MPa)')
ax.set_ylabel('Probability density function')
ax.set_xlim([0, 3.5])
ax.set_ylim([0, 2.5])
ax.grid()
ax.legend()
fig.savefig('../../../thesis/figure/weibull_distribution_size_effect.pdf')
