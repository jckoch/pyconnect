# system packages
import sys
import timeit
import logging as lg

# science packages
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})

# timber connection package
from pyconnect.core import size_effect, estimate_pdf_failure_capacity
from pyconnect.inputs import LoadInputs
from pyconnect.material import Parameters
from pyconnect.connection import Connections
from pyconnect.plotting import plot_MC_samplesize_differences
from pyconnect.plotting import plot_failure_distribution


lg.basicConfig(level=lg.INFO, filename='main.log')


def main(fname, N):
    lg.info('load connection geometries ------------------------')
    inputs = LoadInputs(fname)
    inputs.load_geom_params()
    inputs.load_mat_params()
    lg.info('connection geometries loaded successfully! --------')

    count = sum(map(lambda x : x != 1, inputs.geom_params_length))
    if count < 2:
        fig2, ax2 = plt.subplots(nrows=1, ncols=1, sharey=True,
                                 figsize=(9.3, 3.7))
        ii = 0

    for idn, n in enumerate(N):
        lg.info('load material properties --------------------------')
        m = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=n)
        m.generate_random_variates()
    
        m_se = Parameters(inputs.mat_params, inputs.correlation_matrix, samples=n)
        m_se.generate_random_variates()
        lg.info('material properties loaded successfully! ----------')
    
        lg.info('compute connection capacities ---------------------')
    
        initial_guess = [0.42912284071327017, 0.22015699829905505]
        initial_guess_store = np.zeros((inputs.connections.shape[0], 2))
        advanced = 'yes'
    
        c = Connections(inputs, samples=n)
        c_se = Connections(inputs, samples=n)
        
        if count < 2:      
            f = 0
            num_subplots = 5
            fig1, ax1 = plt.subplots(nrows=1, ncols=num_subplots, sharey=True,
                                     figsize=(9.3, 7.4))

        for i in range(inputs.connections.shape[0]):
            
            c.i = i
            c.N = n
            c.compute_capacity(inputs.connections[i, :], m, advanced)
            lg.info('connection capacities computed successfully -------')
            
            var = inputs.connections[i, 1] / inputs.connections[i, 0]
            titlestr = 'Constant geometric parameters\n$d =$ %.2f and $\\frac{a_3}{d} =$ %.2f and $\\frac{a_4}{d} =$ %.2f' % (
                inputs.connections[i, 0], (inputs.connections[i, 7] / inputs.connections[i, 0]),
                (inputs.connections[i, 8] / inputs.connections[i, 0]))
    
            kde, density = estimate_pdf_failure_capacity(c)
                
            V = inputs.connections[i, -1]
            # tf = initial_guess_mat[:, 2] == V
            # initial_guess = initial_guess_mat[tf, 0:2].reshape((-1,))
            m_se.rand_variates[:, -1], initial_guess = size_effect(V, m_se,
                                                                   initial_guess)
            initial_guess_store[i, :] = initial_guess
    
            c_se.i = i
            c_se.N = n
            c_se.compute_capacity(inputs.connections[i, :], m_se, advanced)
            lg.info('connection capacities computed successfully -------')

            kde_se, density_se = estimate_pdf_failure_capacity(c_se)
        
            if count < 2:
                if i % (inputs.connections.shape[0]/num_subplots) == 0:
                    ax1[f], sd, sb = plot_failure_distribution(ax1[f], V, c,
                                                               c_se, density,
                                                               density_se, var)
                    f += 1
               
                    if f == 5:
                        ax1[0].set_ylabel('$R_{connection}$ [kN]')
                        ax1[0].legend((sd, sb), ('Ductile', 'Brittle'),
                                      loc='upper left')
                        fig1.suptitle(titlestr)
                        if n == 1000:
                            fig1.savefig('../results/%s.pdf' % inputs.name)

        if count < 2:  #i == (inputs.connections.shape[0] - 1):
            ax2 = plot_MC_samplesize_differences(ax2, idn, ii, inputs, c)
            ax2.set_xlabel('$\\frac{t_{1,3}}{d}$ [mm]')
            ax2.set_ylabel('P(B<D)')
            ax2.legend(loc='best', fontsize=14)
            fig2.tight_layout()
            fig2.savefig('../results/%s-samplesizes.pdf' % inputs.name)
            ii += 1

    return(inputs, m, c, initial_guess_store)


if __name__ == '__main__':
    lg.getLogger(__name__)

    # define input filenames containing geometric parameters
    try:
        filenames = sys.argv[1]
    except(IndexError):
        # filenames = ['series-distplots-SF-d8-vary-a3.json',
        #              'series-distplots-SF-d12-vary-a3.json',
        #              'series-distplots-SF-d16-vary-a3.json']
        filenames = ['series-distplots-SF-d12-vary-t1.json']
        # ['series-distplots-SF-d8-vary-t1.json',
        #              'series-distplots-SF-d12-vary-t1.json',
        #              'series-distplots-SF-d16-vary-t1.json']
        #['../data/example-input-series-SF-distplots.json']

    start = timeit.default_timer()
    
    # define other input parameters
    N = [100, 5000, 1000]  # number of realizations to sample material properties with
    
    for fname in filenames:
        inputs, m, c, initial_guess_store = main(fname, N)

    stop = timeit.default_timer()

    total_time = stop - start

    # output running time in a nice format.
    mins, secs = divmod(total_time, 60)
    hours, mins = divmod(mins, 60)

    print("Total running time: %d:%d:%d.\n" % (hours, mins, secs))
