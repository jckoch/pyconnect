from setuptools import setup, find_packages
with open('pyconnect/version.py') as f: exec(f.read())

def readme():
    with open('README.org') as f:
        return f.read()


setup(
    name = "pyconnect",
    install_requires = ['matplotlib >= 1.3.1', 'numpy', 'pandas'],
    python_requires='>=3',
    version = __version__,
    description = "",
    author = "James Koch",
    author_email = "jchkoch@gmail.com",
    packages = find_packages(),
    include_package_data=True,
    url = "",
    entry_points = {'console_scripts': ['pyconnect = pyconnect.pyconnect:main']},
    keywords = [],
    classifiers = [
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 1 - Active",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Education",
        "License :: ",
        'Operating System :: POSIX',
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Scientific/Engineering :: Physics",
        "Topic :: Scientific/Engineering"],
    long_description = readme()
)
