# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})
# from cycler import cycler


def plot_MC_samplesize_differences(ax, idn, ii, inputs, results):
    linestyles = [('densely dotted',        (0, (1, 1))),
                  ('densely dashed',        (0, (5, 1))),
                  ('dotted',                (0, (1, 2.5))),
                  ('dashed',                (0, (5, 2.5))),
                  ('loosely dotted',        (0, (1, 5))),
                  ('loosely dashed',        (0, (5, 5)))]
    colors = ['b', 'g', 'r']
    t1norm = inputs.connections[:, 1] / inputs.connections[:, 0]
    ax.plot(t1norm, results.probability, color=colors[ii], lw=4,
            linestyle=linestyles[idn][1], label='N = %d' % results.N)
    # ax.plot(inputs.connections[:, 1], results.R_cov, color='k',
    #         linestyle=linestyles[idn][1])
    return(ax)


def plot_failure_distribution(ax, vol, r, r_se, density, density_se,
                              var):
    """
    Plot the probability density function of the failure capacity of each
    connection.

    Parameters
    ----------
    ax : matplotlib.axes object
         Axes of the figure to plot the distribution of failure capacities on.
    results : object
              Results of the failure model (i.e. capacities) for a specfic
              connection defined with a unique geometry and variabile material
              properties.
    density : object
              Estimated probability density function of the failure capacity
              distribution.

    Returns
    -------
    ax : matplotlib.axes object
         Histogram of the underlying failure capacity distribution.
    """

    ax.hist(r.R[:, r.i]/1000, bins=100, color='k', alpha=0.25,
               density=True, orientation='horizontal')
    ax.plot(density, r.R[:, r.i]/1000, 'k')
    sd = ax.scatter(0, r.Rd_mean[r.i, :]/1000, color='k', marker='x')
    sb = ax.scatter(0, r.Rb_mean[r.i, :]/1000, color='k', marker='o')

    if vol > 0.075:
        ax.hist(r_se.R[:, r_se.i]/1000, bins=100, color='k',
                    alpha=0.25, density=True, orientation='horizontal')
        ax.plot(density_se, r_se.R[:, r_se.i]/1000, 'k')
        ax.scatter(0, r_se.Rd_mean[r_se.i, :]/1000, color='k', marker='x')
        ax.scatter(0, r_se.Rb_mean[r_se.i, :]/1000, color='k', marker='o')

    ax.set_xticks([], [])
    ax.set_xlabel('$\\frac{t_{1,3}}{d} = $ %.2f mm' % var)

    return(ax, sd, sb)


def plot_sf_results(dataA, dataB, dictionary, keyid):
    key = list(dictionary.keys())

    dataA['t1'] = dataA['t1'] / dataA['d']
    dataA['a1'] = dataA['a1'] / dataA['d']
    dataA['a2'] = dataA['a2'] / dataA['d']
    dataA['a3'] = dataA['a3'] / dataA['d']
    dataA['a4'] = dataA['a4'] / dataA['d']

    dataB['t1'] = dataB['t1'] / dataB['d']
    dataB['a1'] = dataB['a1'] / dataB['d']
    dataB['a2'] = dataB['a2'] / dataB['d']
    dataB['a3'] = dataB['a3'] / dataB['d']
    dataB['a4'] = dataB['a4'] / dataB['d']

    d = dataA['d'].unique()
    a4 = dataA['a4'].unique()

    fig, ax = plt.subplots(4, 1, sharex=True, sharey=True, figsize=(9.3, 11.1))
    fig.subplots_adjust(top=0.825)
    fig1, ax1 = plt.subplots(1, 1, sharex=True, sharey=True, figsize=(9.3, 3.7))
    fig1.subplots_adjust(top=0.85)

    linestyle1 = ['solid', 'dashed']
    linestyle2 = ['dotted', 'dashdot']

    for idy, y in enumerate(dictionary[key[keyid[1]]]):
        subsetA = dataA[round(dataA[key[keyid[1]]], 6) == round(y, 6)]
        subsetA = subsetA.sort_values(by=[key[keyid[0]]])
        subsetB = dataB[round(dataB[key[keyid[1]]], 6) == round(y, 6)]
        subsetB = subsetB.sort_values(by=[key[keyid[0]]])

        vol_mean = np.mean(subsetA['V']) * 100**3

        for idns, ns in enumerate(dictionary[key[-1]]):
            subsetA_ns = subsetA[subsetA['ns'] == ns]

            if idy == 0:
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['P(B<D)'],
                                  color='k', linestyle=linestyle1[idns],
                                  label='P(B<D) and $n_s =$ %d' % ns)
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['COV'],
                                  color='k', linestyle=linestyle2[idns],
                                  label='COV(R) and $n_s =$ %d' % ns)
            elif idy == 1:
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['P(B<D)'],
                             color='k', linestyle=linestyle1[idns])
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['COV'],
                             color='k', linestyle=linestyle2[idns])

                ax1.plot(subsetA_ns[key[keyid[0]]], subsetA_ns['P(B<D)'], lw=4,
                         color='b', linestyle=linestyle1[idns],
                         label='P(B<D) and $n_s =$ %d' % ns)
                ax1.plot(subsetA_ns[key[keyid[0]]], subsetA_ns['COV'], lw=4,
                         color='g', linestyle=linestyle2[idns],
                         label='COV(R) and $n_s =$ %d' % ns)
                ax1.text(0.7, 0.7,
                         '%3.0f$cm^3$' % vol_mean,
                         ha='center', va='center',
                         transform=ax1.transAxes)
                a3_1 = y
            else:
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['P(B<D)'],
                                  color='k', linestyle=linestyle1[idns])
                ax[idy].plot(subsetA_ns[key[keyid[0]]], subsetA_ns['COV'],
                                  color='k', linestyle=linestyle2[idns],
                                  label='COV(R) and $n_s =$ %d' % ns)

        ax[idy].text(0.7, 0.7,
                     '%3.0f$cm^3$' % vol_mean,
                     ha='center', va='center',
                     transform=ax[idy].transAxes)

        subplottitle = key[keyid[1]]
        ax[idy].text(0.5, 1.10,
                          '$\\frac{%s_%s}{d} = $%.3f' % (subplottitle[0],
                                                         subplottitle[1], y),
                          ha="center",
                          transform=ax[idy].transAxes)

        ax[idy].set_ylabel('P(B<D) & COV(R)')

    ax1.set_ylabel('P(B<D) & COV(R)')

    xlabel = key[keyid[0]]
    ax[3].set_xlabel('$\\frac{%s_%s}{d}$' % (xlabel[0], xlabel[1]))
    ax1.set_xlabel('$\\frac{%s_%s}{d}$' % (xlabel[0], xlabel[1]))

    fig.legend(loc='upper center', bbox_to_anchor=(0.5,0.85), ncol=2)
    ax1.legend(loc='best', fontsize=12, ncol=2)

    titlestr = ['Constant geometric parameters: ' +
                '$d =$ %.2f and ' % d +
                '$\\frac{a_4}{d} =$ %.2f ' % a4[0]][0]
    fig.suptitle(titlestr)
    titlestr = ['Constant geometric parameters: ' +
                '$d =$ %.2f and ' % d +
                '$\\frac{a_4}{d} =$ %.2f and ' % a4[0] +
                '$\\frac{%s_%s}{d} = $%.3f' % (subplottitle[0],
                                               subplottitle[1], a3_1)][0]
    fig1.suptitle(titlestr)
    # fig.tight_layout()

    return(fig, fig1)


def plot_mf_results(name, dataA, dataB, dictionary):
    key = list(dictionary.keys())

    dataA['t1'] = dataA['t1'] / dataA['d']
    dataA['a1'] = dataA['a1'] / dataA['d']
    dataA['a2'] = dataA['a2'] / dataA['d']
    dataA['a3'] = dataA['a3'] / dataA['d']
    dataA['a4'] = dataA['a4'] / dataA['d']

    dataB['t1'] = dataB['t1'] / dataB['d']
    dataB['a1'] = dataB['a1'] / dataB['d']
    dataB['a2'] = dataB['a2'] / dataB['d']
    dataB['a3'] = dataB['a3'] / dataB['d']
    dataB['a4'] = dataB['a4'] / dataB['d']

    d = dataA['d'].unique()
    a2 = dataA['a2'].unique()
    a4 = dataA['a4'].unique()

    for idns, ns in enumerate(dictionary[key[9]]):
        dataA_ns = dataA[dataA['ns'] == ns]
        dataB_ns = dataB[dataB['ns'] == ns]

        for idnr, nr in enumerate(dictionary[key[4]]):
            dataA_nr = dataA_ns[dataA_ns['nr'] == nr]
            dataB_nr = dataB_ns[dataB_ns['nr'] == nr]

            for idnc, nc in enumerate(dictionary[key[3]]):
                dataA_nc = dataA_nr[dataA_nr['nc'] == nc]
                dataB_nc = dataB_nr[dataB_nr['nc'] == nc]

                fig, ax = plt.subplots(4, 4, sharex=True, sharey=True,
                                       figsize=(9.3, 7.4))
                fig.subplots_adjust(top=0.75)

                for idx, x in enumerate(dictionary[key[7]]):
                    dataA_x = dataA_nc[dataA_nc[key[7]] == x]
                    dataB_x = dataB_nc[dataB_nc[key[7]] == x]

                    for idy, y in enumerate(dictionary[key[5]]):
                        subsetA = dataA_x[round(dataA_x[key[5]], 6) ==
                                          round(y, 6)]
                        subsetB = dataB_x[round(dataB_x[key[5]], 6) ==
                                          round(y, 6)]

                        for idd, d in enumerate(dictionary[key[0]]):
                            subsetA_d = subsetA[subsetA['d'] == d]
                            subsetA_d = subsetA_d.sort_values(by=['t1'])

                            subsetB_d = subsetB[subsetB['d'] == d]
                            subsetB_d = subsetB_d.sort_values(by=['t1'])

                            vol_mean = np.mean(subsetA_d['V']) * 100**3
                            ax[idx, idy].plot(subsetA_d['t1'],
                                              subsetA_d['P(B<D)'],
                                              color='k', linestyle='solid',
                                              label='P(B<D) w/o. size effect')
                            ax[idx, idy].plot(subsetA_d['t1'],
                                              subsetA_d['COV'],
                                              color='k', linestyle='dotted',
                                              label='COV(R) w/o. size effect')
                            ax[idx, idy].plot(subsetB_d['t1'],
                                              subsetB_d['P(B<D)'],
                                              color='grey', linestyle='solid',
                                              label='P(B<D) w/. size effect')
                            ax[idx, idy].plot(subsetB_d['t1'],
                                              subsetB_d['COV'],
                                              color='grey', linestyle='dotted',
                                              label='COV(R) w/. size effect')

                            ax[idx, idy].text(0.7, 0.7,
                                              '%3.0f$cm^3$' % vol_mean,
                                              ha='center', va='center',
                                              transform=ax[idx, idy].transAxes)

                        if idx == 0:
                            ax[idx, idy].text(0.5, 1.10,
                                              '$\\frac{a_1}{d} = $%.2f' % y,
                                              ha="center",
                                              transform=ax[idx, idy].transAxes)

                        if idy == 3:
                            ax[idx, idy].text(1.05, 0.5,
                                              '$\\frac{a_3}{d} =$%.2f' % x,
                                              va="center",
                                              transform=ax[idx, idy].transAxes)

                        if idx == 3:
                            ax[idx, idy].set_xlabel('$\\frac{t_{1,3}}{d}$')

                        if idy == 0:
                            ax[idx, idy].set_ylabel('P(B<D) & COV(R)',
                                                    fontsize=9)

                        if (idx == 0) & (idy == 0):
                            fig.legend(loc='upper center',
                                       bbox_to_anchor=(0.5,0.875),
                                       fontsize=10, ncol=4)

                    titlestr = ['Constant geometric parameters: ' +
                                '$d =$ %.2f and ' % d +
                                '$\\frac{a_2}{d} =$ %.2f and ' % a2[0] +
                                '$\\frac{a_4}{d} =$ %.2f\n' % a4[0] +
                                'Number of fasteners: ' +
                                '$n_c =$ %d and ' % nc +
                                '$n_r = $ %d and ' % nr +
                                'Number of steel plates: ' +
                                '$n_s =$ %d' % ns][0]
                    fig.suptitle(titlestr)
                    fig.savefig('../results/%s-nr=%d-nc=%d-ns=%d.pdf' % (name, nc, nr, ns))

    return(fig)
