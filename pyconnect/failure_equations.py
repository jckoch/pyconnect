# failure_equations.py

import numpy as np


def european_yield_model(fh, d, t1, My):
    """
    Ductile (EYM) failure modes for steel-wood connections. Minimum requirement of a
    double shear connection steel plates as either connection member. Internal steel plates
    are supported. External steel plates for 3-member connections are also supported; however,
    multi-member connections with external steel plates are currently NOT supported.

    Input:
    - d: fastener diameter
    - t: timber member thickness (either t1, side, or t2, main, member)
    - fh: embedment strength of timber members
    - My: yield moment of fastener

    Output:
    - Fv: ductile (EYM) failure capacities based on 3 base failure mechanisms

    Notes:
    Valid for internal steel plate connections of any number of members and only valid for
    external steel plate 3-member connections.
    """
    Fv = np.array([fh * d * t1,
                   0.5 * fh * d * t1,
                   fh * d * t1 * (np.sqrt(2 + (4 * My) / (fh * d * t1**2)) - 1),
                   2 * np.sqrt(My * fh * d)]).transpose()
    #modes_ductile = ['mode Ia', 'mode Ib', 'mode II', 'mode III']
    return(Fv)


def splitting(t, a1, a2, a3, a4, nr, nc, b, fv, ft0, ft90):
    """
    Splitting failure of timber connections based on relations
    developed in Jorissen 1998.

    Inputs:
    - t: timber member thickness (should consider all members)
    - a3: end spacing of fastener row to edge of timber member
    - fv: shear strength of timber
    - ft90: tensile strength of timber perp. to grain

    Output:
    - Fv: brittle splitting capacity for shear and tensile governing failures

    Notes:
    kv, kt90 are stress distribution based factors around fasteners set to
    constant values based on Jorissen 1998.
    """
    kv, kt0, kt90 = [0.5, 1.25, 1]

    if a1 != 0:
        aLmin = min(a1, a3)
    else:
        aLmin = a3

    if a2 != 0:
        bnet = b - 2 * a4
    else:
        bnet = b

    Fv = np.array([kt90 * (1 / 0.143) * t * a3 * ft90,   # splitting
                   2 * kv * nr * nc * t/2 * aLmin * fv,  # row shear
                   kt0 * bnet * t * ft0]).transpose()    # tensile head plane
    return(Fv)


################################################################################
# NOT IMPLEMENTED YET


#def steelwoodsteel_thickplates(fh, d, t2, My):
#    """
#    Failure mode j and k: Steel to timber connection
#    - in double shear
#    - external _thick_ steel plates
#    """
#    Fv = np.array([0.5 * fh * t2 * d,
#                   np.sqrt(My * fh * d)])
#    modes_ductile = ['mode Ia', 'mode III']
#    return(Fv, modes_ductile)


#def steelwoodsteel_thinplates(fh, d, t2, My):
#    """
#    Failure mode l and m: Steel to timber connection
#    - in double shear
#    - external _thin_ steel plates
#    """
#    Fv = np.array([0.5 * fh * t2 * d,
#                   np.sqrt(2 * My * fh * d)])
#    modes_ductile = ['mode Ib', 'mode II']
#
#    return(Fv, modes_ductile)


#def rowshear(t, nc, nr, a1, fv):
#    """
#    Row shear failure of timber connections based on relations
#    developed in Quenneville 2000.
#
#    Input:
#    - t: timber member thickness (should consider all members)
#    - nc: number of fasteners in a row
#    - nr: number of fastener rows
#    - a1: spacing of fasteners within a row
#    - fv: shear strength of timber
#    - kLS: fastener load distribution factor
#
#    Output:
#    - Fv: brittle row-shear capacity for shear governing failures
#    """
#    kLS = 0.65  # 1.00 for main (middle) members
#    F_L = 0.75 * kLS * nc * a1 * t * fv
#    Fv = np.array([2 * nr * F_L]).transpose()
#    return(Fv)


#def blockshear(t, nc, nr, a1, a2, a4, d, fv, ft0):
#    """
#    Block shear failure of timber connections based on relations
#    developed by Quenneville (2000).
#
#    Input:
#    - t: timber member thickness (should consider all members)
#    - nc: number of fasteners in a row
#    - nr: number of fastener rows
#    - a1: spacing of fasteners within a row
#    - a2: spacing of fasteners between rows
#    - a4: edge spacing between row and edge of connection
#    - d: diameter of fastener
#    - fv: shear strength of timber
#    - ft0: parallel to grain tension strength of timber
#    - kLS: fastener load distribution factor
#
#    Output:
#    - Fv: brittle row-shear capacity for shear governing failures
#    """
#    kLS = 0.65  # 1.00 for main (middle) members
#    F_L = 0.75 * kLS * nc * a1 * t * fv
#    bnet = nr * d + (nr - 1) * a2  # net tension x-section (remove fastener diameter)
#    F_H = 1.25 * bnet * t * ft0
#    Fv = np.array([F_L + 2 * F_H]).transpose()
#    return(Fv)


# def singleshear(self):
#     """
#     """
#     beta = fh[1] / fh[0]
#     tfrac = t2 / t1
#     Fv = np.array([fh[0] * t1 * d,
#                    fh[1] * t2 * d,
#                    fh[0] * t2 * d * (np.sqrt(beta + 2 * beta**2 * (1 + tfrac + tfrac**2) +
#                                                             beta**3 * tfrac**2) - beta * (1 + tfrac)),
#                 ((fh[0] * t1 * d) / (2 + beta)) * (np.sqrt(2 * beta * (1 + beta) +
#                                                                           ((4 * beta * (2 + beta)) * My) /
#                                                                           (fh[0] * d * t1**2)) - beta),
#                    ((fh[0] * t1 * d) / (1 + 2 * beta)) * (np.sqrt(2 * beta**2 * (1 + beta) +
#                                                                                  ((4 * beta * (2 + beta)) * My) /
#                                                                                  (fh[0] * d * t2**2)) - beta),
#                    np.sqrt((2 * beta) / (1 + beta)) * np.sqrt(2 * My * fh[0] * d)])
#     return(Fv)


# def doubleshear(self):
#     """
#     """
#     beta = fh[1] / fh[0]
#     Fv = np.array([fh[0] * t1 * d,
#                    0.5 * fh[1] * t2 * d * beta,
#                    (((fh[0] * t1 * d) / (2 + beta)) *
#                     (np.sqrt(2 * beta * (1 + beta) +
#                              (4 * beta * (2 + beta) * My) /
#                              (fh[0] * d * t1**2)) - beta)),
#                    (np.sqrt(2 * beta / (1 + beta)) *
#                     np.sqrt(2 * My * fh[0] * d))])
#     return(Fv)
