import numpy as np
from scipy.stats import norm, lognorm, gumbel_l, weibull_min
from scipy.linalg import cholesky

from pyconnect.posdef import nearestPD
from pyconnect.core import dist_parameters, standard_normal


class Parameters():
    """
    Compute material properties as random variables based
    on probabilistic input data capturing the material uncertainties.
    """

    def __init__(self, mat_params, correlation_matrix, samples=1000):
        """
        Initialize material data values.
        """

        mat_params = mat_params.to_dict('index')
        self.mat_params = dist_parameters(mat_params)
        self.cor_matrix = correlation_matrix

        # define standard normal random variates for each material property
        self.rv_norm = standard_normal(samples, self.mat_params)
        self.cor_matrix = nearestPD(self.cor_matrix)  # ensure cor_matrix is positive definite

        # define cholesky factorized cross-correlation between RV using cor_matrix
        chol_matrix = cholesky(self.cor_matrix, lower=False)

        # perform cross-correlation on RVs
        self.rv_norm = self.rv_norm @ chol_matrix

        # define output RV array
        self.rand_variates = np.zeros(self.rv_norm.shape)


    def generate_random_variates(self):
        """
        Create random variables of input uncertain properties.
        """

        char_val = np.zeros([1, self.rv_norm.shape[1]])  # initialize char. value array
        c = 0  # counter
        for _, value in self.mat_params.items():
            uniform_margin = norm.cdf(self.rv_norm[:, c])
            if value['Dist. Type'] == 'normal':
                self.rand_variates[:, c] = norm(loc=value['mu'],
                                                scale=value['sigma']).ppf(
                                                    uniform_margin)
                mu, sigma = norm.fit(self.rand_variates[:, c])
                rv = norm(loc=mu, scale=sigma)
                char_val[:, c] = rv.ppf(value['Percentile'])
            elif value['Dist. Type'] == 'lognormal':
                self.rand_variates[:, c] = lognorm(value['sigma'],
                                                   loc=0,
                                                   scale=np.exp(
                                                       value['mu'])).ppf(
                                                           uniform_margin)
                s, loc, scale = lognorm.fit(self.rand_variates[:, c])
                rv = lognorm(s, loc=loc, scale=scale)
                char_val[:, c] = rv.ppf(value['Percentile'])
            elif value['Dist. Type'] == 'weibull':
                self.rand_variates[:, c] = weibull_min(value['k'],
                                                       loc=0,
                                                       scale=value['lambda']
                                                       ).ppf(uniform_margin)
                k, _, Lambda = weibull_min.fit(self.rand_variates[:, c])
                rv = weibull_min(k, loc=0, scale=Lambda)
                char_val[:, c] = rv.ppf(value['Percentile'])
            elif value['Dist. Type'] == 'gumbel':
                # DONE: Check the accuracy of this!!! Specifically, if it is possible to avoid the ``fit''
                #       function in scipy and directly generate the distribution using the distribution parameters
                #       of the mode, mu, and scale parameter, beta. (not urgent as I am not using this at the moment)
                self.rand_variates[:, c] = gumbel_l(loc=value['mu'],
                                                    scale=value['beta']).ppf(
                                                        uniform_margin)
                mu, beta = gumbel_l.fit(self.rand_variates[:, c])
                rv = gumbel_l(loc=mu, scale=beta)
                char_val[:, c] = rv.ppf(value['Percentile'])
            else:
                print('Unsupported distribution!!!')

            c += 1  # increase counter

        self.mat_params_names = list(self.mat_params.keys())
        self.char_values = char_val
