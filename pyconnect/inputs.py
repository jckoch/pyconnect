# input.py

import numpy as np
import pandas as pd

from pyconnect.core import load_input


class LoadInputs:
    """
    Class object to store and load input data for parametric study of
    structural timber connections.

    Inputs are to be provided in a json formatted file. Geometric parameters
    which are possibly varied within the spatial domain are directly specified
    within the input file. Material data is included by reference of filenames
    to files containing the material input parameters. These filenames should
    either be specified as absolute paths or relative paths.
    """

    def __init__(self, fname):
        """
        Initialize input variables for parametric study.
        """
        data = load_input(fname)
        self.name = data['name']
        self.connection_type = data['type of connection']
        self.geom_params = data['geometrical parameters']
        self.mat_datafiles = data['material parameters']

    def load_geom_params(self):
        """
        Load the input geometric parameters and generate the set of connections
        (i.e. the geometric domain) of interest with respect to the input
        parameters.
        """

        # compute evenly spaced values for each parameter, if <num> equals to 1
        # then parameter only is given one value
        self.geom_params_length = []
        for key, val in self.geom_params.items():
            self.geom_params[key] = np.linspace(*val)
            self.geom_params_length.append(val[2])

        # define set of connections
        self.connections = np.array(np.meshgrid(self.geom_params['d'],  #0
                                                self.geom_params['t1'], #1
                                                self.geom_params['t2'], #2
                                                self.geom_params['nc'], #3
                                                self.geom_params['nr'], #4
                                                self.geom_params['a1'], #5
                                                self.geom_params['a2'], #6
                                                self.geom_params['a3'], #7
                                                self.geom_params['a4'], #8
                                                self.geom_params['ns']) #9
                                    ).T.reshape(-1, len(self.geom_params))
        
        # replace normalized variables with real values
        # (e.g. using formula of "var = norm_var * d")
        param2num = {'t1': 1, 'a1': 5, 'a2': 6, 'a3': 7, 'a4': 8}
        for key, val in param2num.items():
            if key == 't1':
                replace = self.connections[:, val] * self.connections[:, 0]
            elif key ==  'a1':
                replace = self.connections[:, val] * self.connections[:, 0]                
            elif key == 'a2':
                replace = self.connections[:, val] * self.connections[:, 0]
            elif key == 'a3':
                replace = self.connections[:, val] * self.connections[:, 0]
            elif key == 'a4':
                replace = self.connections[:, val] * self.connections[:, 0]
            
            self.connections[:, val] = replace
                                
        # compute volume of each connection
        self.connections = connection_volume(self.connections)
        
        # return list of names of each column
        self.geom_param_names = list(self.geom_params.keys())
        self.geom_param_names.append(['b', 'V'])
        
        # save pre-sorted by volume connection matrix
        self.connections_original = self.connections


    def load_mat_params(self):
        try:
            read_matfiles(self)
        except(FileNotFoundError):
            read_matfiles(self, prepend='../')           
            print('FileNotFoundError: Could not locate file.')
            print('Using default input file!!!')


def read_matfiles(self, prepend='./'):
    """
    Read material data files from filenames listed in input json file.

    Returns
    -------
    None.
    """

    fname_mat_params = prepend + self.mat_datafiles['material data']
    self.mat_params = pd.read_csv(fname_mat_params, index_col=0)
    if 'correlation matrix' in self.mat_datafiles:
        fname_cor_matrix = prepend + self.mat_datafiles['correlation matrix']
        self.correlation_matrix = pd.read_csv(fname_cor_matrix, index_col=0)
        self.correlation_matrix = self.correlation_matrix.to_numpy()
    else:
        print('No correlation matrix was given.\n')
        print('Using the identity matrix.')
        self.correlation_matrix = np.identity(len(self.mat_params))


def connection_volume(connections):
    """
    Calculate the connection volume.

    Parameters
    ----------
    connections : numpy.ndarray
        Input connection properties.

    Returns
    -------
    connections.
    """

    Lc = (2 * connections[:, 7] +
          connections[:, 3] * connections[:, 0] +
          (connections[:, 3] - 1) * connections[:, 5])
    b = (2 * connections[:, 8] +
         connections[:, 4] * connections[:, 0] +
         (connections[:, 4] - 1) * connections[:, 6])
    tc = np.zeros(Lc.shape)

    for i in range(len(connections[:, 9])):
        if connections[i, 9] == 2:
            tc[i] = (connections[i, 9] * connections[i, 1] +
                        connections[i, 9]/2 * connections[i, 2])
        else:
            tc[i] = ((connections[i, 9] - 1) * connections[i, 1] +
                        connections[i, 9]/2 * connections[i, 2])

    V = (Lc * b * tc) / 1000**3
    connections = np.column_stack((connections, b, V))

    return(connections)
