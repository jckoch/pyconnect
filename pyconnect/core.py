# core.py
import json
import numpy as np
import sympy as sp
from scipy.stats import norm, weibull_min
from scipy.stats import gaussian_kde


def load_input(fname):
    with open(fname, 'r') as read_file:
        data = json.load(read_file)
    return(data)


def dist_parameters(dictionary):
    for key, value in dictionary.items():
        if value['Dist. Type'] == 'normal':
            dictionary[key]['mu'] = value['Mean Value']
            # standard deviation NOT variance
            dictionary[key]['sigma'] = value['Mean Value'] * value['COV']
        elif value['Dist. Type'] == 'lognormal':
            m = value['Mean Value']
            v = (value['Mean Value'] * value['COV'])**2
            dictionary[key]['mu'] = np.log(m**2/np.sqrt(v+m**2))
            dictionary[key]['sigma'] = np.sqrt(np.log(v/m**2+1))
        elif value['Dist. Type'] == 'weibull':
            m = value['Mean Value']
            v = (value['Mean Value'] * value['COV'])**2
            Lambda, k, _ = weibull_parameters(m, v)
            dictionary[key]['lambda'] = Lambda  # scale parameter
            dictionary[key]['k'] = k            # shape parameter
        elif value['Dist. Type'] == 'gumbel':
            m = value['Mean Value']
            v = (value['Mean Value'] * value['COV'])**2
            dictionary[key]['beta'] = np.sqrt(6 * v / np.pi**2)
            dictionary[key]['mu'] = m + 0.57721 * dictionary[key]['beta']
        else:
            Warning('Unsupported distribution type!!!')
    return(dictionary)


def standard_normal(N, parameters):
    X = np.zeros([N, len(parameters)])
    for i in range(X.shape[1]):
        X[:, i] = norm(loc=0, scale=1).rvs(N)
    return(X)


def weibull_parameters(m, v, initial_guess=[0.7297, 0.3932]):
    """
    Symbolic numerical procedure to derive the parameters
    of a Weibull 2-parameter distribution given the aggregate
    statistics of the mean, m, and variance, v.
    """
    x, alpha, betai, mu, sigma2 = sp.symbols('x alpha betai mu sigma2')

    expr1 = alpha**(-1*betai) * sp.gamma(1+betai)
    expr2 = alpha**(-2*betai) * (sp.gamma(1+2*betai) - (sp.gamma(1+betai))**2)

    eqn1 = sp.Eq(expr1, m)
    eqn2 = sp.Eq(expr2, v)
    eqns = [eqn1, eqn2]

    Si = sp.nsolve(eqns, [alpha, betai], initial_guess)

    Lambda = float(Si[0]**(-Si[1]))
    k = float(1/Si[1])

    initial_guess = [float(Si[0]), float(Si[1])]

    return(Lambda, k, initial_guess)


def size_effect(V, m, initial_guess):
    """
    Compute mean strength of variables affected by the "size effect" using the
    weakest link theory. Note that the default shape parameter of the
    Weibull distribution used is 4.542213092139039 
    (inverse: 0.22015699829905505).

    Parameters
    ----------
    V : float
        Volume of the connection under consideration.

    Returns
    -------
    f2 : Adjusted mean strength based on volume of connection.
    """

    f0 = m.mat_params['ft90']['Mean Value']
    V0 = 0.01
    k0 = m.mat_params['ft90']['k']

    f2 = f0 / (V / V0)**(1 / k0)
    f2_var = (f2 * m.mat_params['ft90']['COV'])**2

    Lambda, k, initial_guess = weibull_parameters(f2, f2_var,
                                                  initial_guess=initial_guess)
    uniform_margin = norm.cdf(m.rv_norm[:, -1])
    ft90_variate = weibull_min(k, loc=0, scale=Lambda).ppf(uniform_margin)

    return(ft90_variate, initial_guess)


def estimate_pdf_failure_capacity(results):
    """
    Estimate the probability density function of the distribution of the 
    failure capacity of each connection.

    Parameters
    ----------
    results : object
              Results of the failure model (i.e. capacities) for a specfic
              connection defined with a unique geometry and variabile material
              properties.

    Returns
    -------
    kde : object
          Scipy stats object to represent the kernel density estimator of the
          distribution of failure capacities for the connection.
    density : Evaluated probability density function to represent the
              distribution of the failure capacity of the connection.
    """

    kde = gaussian_kde(results.R[:, results.i]/1000)
    density = kde(results.R[:, results.i]/1000)

    return(kde, density)


