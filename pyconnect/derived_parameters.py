# derived_parameters.py

# Notes:
# regression parameters A, B, C, and D. See Leijten 2004 (CIB-W18/37-7-12)

import numpy as np
from scipy.stats import norm

from pyconnect.posdef import nearestPD
from pyconnect.core import standard_normal


def embedment_strength(N, d, rho, advanced='yes'):
    """
    Embedment strength of dowel-type
    fasteners as a function of dowel diameter and
    timber material density
    """

    if advanced == 'yes':
        parameters = np.array([[-2.334, 0.232],
                               [1.066, 0.038], 
                               [-0.253, 0.012],
                               [0.107, 0.003]])
        rv_norm = standard_normal(N, parameters)
        cor_matrix = np.array([[1, -0.99, -0.24, 0],
                               [-0.99, 1, 0.11, 0],
                               [-0.24, 0.11, 1, 0],
                               [0, 0, 0, 1]])
        cor_matrix = nearestPD(cor_matrix)
        rv_norm = rv_norm @ cor_matrix
        rand_variate = np.zeros(rv_norm.shape)
        for i in range(len(parameters)):
            uniform_margin = norm.cdf(rv_norm[:, i])
            mu, sigma = parameters[i, :]
            rand_variate[:, i] = norm(loc=mu, scale=sigma).ppf(uniform_margin)
            
            # A = lognorm(pA[1], loc=0, scale=np.exp(pA[0])).rvs(1000)
            # B = norm(pB[0], pB[1]).rvs(1000)
            # C = norm(pC[0], pC[1]).rvs(1000)
            # D = norm(pD[0], pD[1]).rvs(1000)

        fh = (rand_variate[:, 0] + rand_variate[:, 1] * np.log(rho) +
              rand_variate[:, 2] * np.log(d) + rand_variate[:, 3])
        fh = np.exp(fh).reshape((-1, 1))
    else:
        fh = (0.082 * (1 - 0.01 * d) * rho).reshape(len(rho), 1)

    return(fh)


def yield_moment(d, fu):
    """
    Yield moment of the dowel as a function of the
    fastener yield strength and dowel diameter.
    """
    My = (0.3 * fu * d**2.6).reshape(len(fu), 1)
    return(My)
