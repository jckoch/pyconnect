import numpy as np
from scipy.stats import norm, gaussian_kde

from pyconnect.derived_parameters import embedment_strength, yield_moment
from pyconnect.failure_equations import european_yield_model, splitting


class Connections:
    """
    Object to contain all capacity calculations for a connection with one
    set of geometric properties. The connection capacity is computed for all
    simulated probabilistic material property values. This class object will
    also save the aggregate statistics of each connection (with unique
    geometrical properties) in a data structure of an array. Each row of the
    array will contain the mean and COV of the failure capacity of each mode.
    """

    def __init__(self, inputs, samples=1000):
        self.connection_type = inputs.connection_type
        self.kLS = [0.65, 1]

        self.R = np.zeros((samples, inputs.connections.shape[0]))
        self.R_mean = np.zeros((inputs.connections.shape[0], 1))
        self.R_cov = np.zeros((inputs.connections.shape[0], 1))

        self.Rd = np.zeros((samples, inputs.connections.shape[0]))
        self.Rd_mean = np.zeros((inputs.connections.shape[0], 1))
        self.Rd_cov = np.zeros((inputs.connections.shape[0], 1))
        self.Rd_rv = []

        self.Rb = np.zeros((samples, inputs.connections.shape[0]))
        self.Rb_mean = np.zeros((inputs.connections.shape[0], 1))
        self.Rb_cov = np.zeros((inputs.connections.shape[0], 1))
        self.Rb_rv = []

        self.probability = np.zeros((inputs.connections.shape[0], 1))


    def compute_capacity(self, geom, material, advanced):
        self.rv, _ = self.derived_properties(self, geom[0],
                                             material.rand_variates,
                                             material.mat_params_names,
                                             advanced)
        R_ductile = self.ductile_capacity(self, geom)
        self.Rd[:, self.i] = R_ductile
        R_brittle = self.brittle_capacity(self, geom)
        self.Rb[:, self.i] = R_brittle

        mu_ductile, sigma_ductile = norm.fit(R_ductile)
        self.Rd_rv.append(norm(mu_ductile, sigma_ductile))
        self.Rd_mean[self.i, 0] = mu_ductile
        self.Rd_cov[self.i, 0] = sigma_ductile / mu_ductile

        mu_brittle, sigma_brittle = norm.fit(R_brittle)
        self.Rb_rv.append(norm(mu_brittle, sigma_brittle))
        self.Rb_mean[self.i, 0] = mu_brittle
        self.Rb_cov[self.i, 0] = sigma_brittle / mu_brittle

        Rd_min = R_ductile
        Rb_min = R_brittle
        R = np.sort(Rb_min - Rd_min)
        x = np.linspace(0, 1, len(R))
        if x[R < 0].shape[0] == 0:
            self.probability[self.i, 0] = 0.0
        else:
            self.probability[self.i, 0] = x[R < 0][-1]

        R = np.min(np.array([Rd_min, Rb_min]).T, axis=1)
        self.R[:, self.i] = R
        self.R_mean[self.i, 0] = np.mean(R)
        self.R_cov[self.i, 0] = np.std(R) / np.mean(R)


    # utility functions
    @staticmethod
    def derived_properties(self, d, RV, RV_names, advanced):
        RV = np.append(RV, embedment_strength(self.N, d, RV[:, 0], advanced), axis=1)
        RV_names.append('fh')
        RV = np.append(RV, yield_moment(d, RV[:, 1]), axis=1)
        RV_names.append('My')
        return(RV, RV_names)

    @staticmethod
    def ductile_capacity(self, params):
        """
        Wrapper function to compute the ductile capacity of timber connections
        according to the European Yield Model. Takes as input the type of
        connection, geometric parameters, and material properties as vectors of
        N-sampled random variables. Output consists of a array consisting of
        each column representing a failure mode and the rows representing each
        N-sampled random variable material property.

        Notes: Limit to symmetric steel timber connections. This means that the
        timber member thicknesses is equal for all timber parts including in
        multiple shear plane connections.
        """

        # define input variables from input argument
        d, t1, t2, nc, nr, a1, a2, a3, a4, ns, b, V = params

        # define material properties required from input argument
        fh = self.rv[:, 5]
        My = self.rv[:, 6]

        # initialize output variable
        R = np.array((self.rv.shape[0], 1))

        # define multi-fastener reduction factor (load redistribution)
        if a1 != 0:
            nef = min(nc * nr, (nc * nr)**0.9 * np.sqrt(a1 / (13 * d)))
        else:
            nef = nc * nr

        # compute capacity
        Fv = european_yield_model(fh, d, t1, My)


        if ns > 1:  # multiple steel plates
            if self.connection_type == 'sts':  # external steel plates
                print('Not implemented')
            else:                              # multiple internal steel plates
                Router = np.min(np.array([Fv[:, 0] + Fv[:, 1], # mode Ia+mode Ib
                                          Fv[:, 0] + Fv[:, 3], # mode Ia+mode III
                                          Fv[:, 1] + Fv[:, 2], # mode Ib+mode II
                                          Fv[:, 1] + Fv[:, 3], # mode Ib+mode III
                                          Fv[:, 2] + Fv[:, 3], # mode II+mode III
                                          Fv[:, 3] + Fv[:, 3]]).T, # mode III+mode III
                                axis=1)
                Rinner = np.min(np.array([Fv[:, 1] + Fv[:, 1],      # mode Ib + mode Ib
                                          Fv[:, 3] + Fv[:, 3]]).T,  # mode III + mode III
                                axis=1)
                R = np.add(Router * 2, Rinner * (ns - 2))
        else:                                  # single internal steel plate
            R = np.min(np.array([Fv[:, 0] + Fv[:, 0],      # mode I + mode I
                                 Fv[:, 2] + Fv[:, 2],      # mode II + mode II
                                 Fv[:, 3] + Fv[:, 3]]).T,  # mode III + mode III
                       axis=1)

        R = nef * R

        return(R)

    @staticmethod
    def brittle_capacity(self, params):
        """
        Wrapper function to compute the brittle capacity of timber connections
        according to the European Yield Model. Takes as input the type of
        connection, geometric parameters, and material properties as vectors of
        N-sampled random variables. Output consists of a array consisting of
        each column representing a failure mode and the rows representing each
        N-sampled random variable material property.
        """

        d, t1, t2, nc, nr, a1, a2, a3, a4, ns, b, V = params
        fv = self.rv[:, 2]
        ft0 = self.rv[:, 3]
        ft90 = self.rv[:, 4]

        # initialize output variable
        R = np.array((self.rv.shape[0], 1))

        if self.connection_type == 'sts':
            Fv = splitting(t1, a1, a2, a3, a4, nc, b, fv, ft0, ft90)
        elif self.connection_type == 'tst':
            if ns > 1:
                t1 = min(t1, t1/2)

            Fv = splitting(t1, a1, a2, a3, a4, nr, nc, b, fv, ft0, ft90)
        else:
            raise(ValueError)

        R = np.min(np.array([Fv[:, 0],                           # splitting
                             Fv[:, 1],                           # row shear
                             Fv[:, 2],                           # net tension
                             2 * Fv[:, 1] + Fv[:, 2]]).T,        # block shear
                   axis=1)

        return(R)

    def save_results(self, counter, fname):
        self.Rd[:, counter] = self.R_ductile
        self.Rb[:, counter] = self.R_brittle
        self.R[:, counter] = np.min(np.array([self.R_ductile, self.R_brittle]),
                                    axis=0)

        np.savetxt(fname[0], self.Rd, delimiter=',')
        np.savetxt(fname[1], self.Rb, delimiter=',')
        np.savetxt(fname[2], self.R, delimiter=',')
