from unittest import TestCase
import numpy as np
from pyconnect.connection import Connection


class TestConnection(TestCase):
    """
    Test calculation of steel-timber connection capacities
    """

    def setUp(self):
        type_of_connection = 'tst'
        params = {'d': 12, 't1': 154, 't2': 200, 'a1': 120, 'a3': 100, 'nc': 1, 'nr': 1}
        mparams = {'rho': 455, 'fu': 800, 'fv': 5, 'ft90': 2}
        self.c = Connection(type_of_connection, params)
        self.c.derived_material_properties(mparams)
        self.c.capacity()


    def test_class_variables_assigned_correctly(self):
        self.assertEqual(self.c.d, 12)
        self.assertEqual(self.c.t1, 154)
        self.assertEqual(self.c.t2, 200)
        self.assertEqual(self.c.a1, 120)
        self.assertEqual(self.c.a3, 100)
        self.assertEqual(self.c.nc, 1)
        self.assertEqual(self.c.nr, 1)


    def tests_class_input_args_return_error_if_mismatch_in_type(self):
        self.assertRaises(ValueError, Connection, 100, [])
        self.assertRaises(ValueError, Connection, {}, 100)


    def test_class_variables_are_of_correct_type(self):
        self.assertIsInstance(self.c.d, (int, float))
        self.assertIsInstance(self.c.t1, (int, float))
        self.assertIsInstance(self.c.t2, (int, float))
        self.assertIsInstance(self.c.a1, (int, float))
        self.assertIsInstance(self.c.a3, (int, float))
        self.assertIsInstance(self.c.nc, (int, float))
        self.assertIsInstance(self.c.nr, (int, float))


    def test_compute_derived_material_properties_returns_correct_result(self):
        self.assertAlmostEqual(self.c.fh, 32.8328, 4)
        self.assertAlmostEqual(self.c.My, 153490.8466, 4)
        self.assertEqual(self.c.fv, 5)
        self.assertEqual(self.c.ft90, 2)


    def test_compute_derived_material_properties_returns_error_if_mparam_not_dict(self):
        self.assertRaises(ValueError, self.c.derived_material_properties, [])


    def test_compute_derived_material_properties_returns_error_if_mparam_values_not_numbers(self):
        self.assertRaises(ValueError, self.c.derived_material_properties, {'rho': '455', 'fu': '800'})


    def test_compute_derived_material_properties_returns_error_if_mparam_arg1_not_number(self):
        self.assertRaises(ValueError, self.c.derived_material_properties, {'rho': '455', 'fu': 800})


    def test_compute_derived_material_properties_returns_error_if_mparam_arg2_not_number(self):
        self.assertRaises(ValueError, self.c.derived_material_properties, {'rho': 455, 'fu': '800'})


    def test_compute_ductile_capacity_returns_correct_result(self):
        self.assertEqual(self.c.R_ductile.all(), np.array([60675.0144, 30337.5072, 26530.56105, 15553.05902]).all())


    def test_compute_brittle_capacity_returns_correct_result(self):
        self.assertEqual(self.c.R_brittle.all(), np.array([77000, 215384.6154, 90090]).all())
