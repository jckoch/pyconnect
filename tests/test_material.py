import unittest
import numpy as np
from pyconnect.material import Parameters


class TestParameters(unittest.TestCase):
    """
    Test case for Parameters object.
    """
    @classmethod
    def setUp(self):
        np.random.seed(23335)
        self.fname = 'data/example_parameters_loads.csv'
        self.var = Parameters(self.fname)
        self.var.generate_random_variates(samples=1)


    def test_init(self):
        self.assertIsInstance(self.var.parameters, dict)


    def test_dist_parameters(self):
        self.assertEqual(self.var.parameters['G']['mu'], 20000)
        self.assertEqual(self.var.parameters['G']['sigma'], 2000)
        self.assertAlmostEqual(self.var.parameters['Q']['mu'], 13924.2302918)
        self.assertAlmostEqual(self.var.parameters['Q']['beta'], 3680.16890183)


    def test_random_variates(self):
        self.assertEqual(len(self.var.dX), 1)
        np.testing.assert_array_almost_equal(self.var.dX.loc[0].to_list(),
                                             np.array([21361.19389506,
                                                       16202.51567092]))
        #self.assertEqual(self.var.parameters['G']['Char. Value'], 20000)
        #np.testing.assert_array_almost_equal(self.var.X[:, 1],
        #                                     np.array([15164.479885,
        #                                               14316.586669]))\
        #self.assertEqual(self.var.parameters['Q']['Char. Value'], )
